import cv2 as cv
import numpy as np

def flatten(lis):
     for item in lis:
        if hasattr(item, "__iter__") and not isinstance(item, str):
            for x in flatten(item):
                yield x
        else:        
             yield item

def get_lines(image):
    src = cv.cvtColor(image,cv.COLOR_RGB2GRAY)

    dst = cv.Canny(src, 50, 200, None, 3)

    lines = cv.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)

    my_lines = []

    if lines is not None:
        for i in range(0, len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
            pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
            my_lines.append((pt1, pt2))
    return np.array(list(flatten(my_lines)))
