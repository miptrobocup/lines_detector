#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
from rospy.numpy_msg import numpy_msg
from rospy_tutorials.msg import Floats

import get_lines


bridge = CvBridge()

def prepare_img(message):
    cv2_img = bridge.imgmsg_to_cv2(message, "bgr8")
    img = np.array(cv2_img)

    # ! check dimensions and transpose if need
    rospy.loginfo("input image shape: ")
    return img


def create_detector(publisher):
    def process_image(message):
        img = prepare_img(message)
        
        rospy.loginfo('starting detection')
        detections = get_lines.get_lines(img)
        rospy.loginfo('finised detection')
        rospy.loginfo(detections)
        rospy.loginfo("started publishing")
        publisher.publish(detections)
        rospy.loginfo("finished publishing")
    
    return process_image


def listener():
    rospy.init_node('listener', anonymous=True)
    
    
    detections_publisher = rospy.Publisher('lines_detections', Floats, queue_size=10) # ! tune queue_size later
    
    detector_callback = create_detector(detections_publisher)
    
    rospy.Subscriber("/naoqi_driver/camera/front/image_raw", Image, detector_callback)

    rospy.spin()             

if __name__ == '__main__':
    listener()

