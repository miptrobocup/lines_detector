import cv2 as cv
import numpy as np

def get_lines(filename):
    src = cv.imread(filename, cv.IMREAD_GRAYSCALE)

    dst = cv.Canny(src, 50, 200, None, 3)

    lines = cv.HoughLines(dst, 1, np.pi / 180, 150, None, 0, 0)

    my_lines = []

    if lines is not None:
        for i in range(0, len(lines)):
            rho = lines[i][0][0]
            theta = lines[i][0][1]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a * rho
            y0 = b * rho
            pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
            pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
            my_lines.append((pt1, pt2))
    return np.array(my_lines)
